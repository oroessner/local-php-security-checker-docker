FROM alpine:latest

ARG CHECKER_VERSION=1.2.0
ARG TARGETPLATFORM

RUN set -eux; \
   wget -O /local-php-security-checker "https://github.com/fabpot/local-php-security-checker/releases/download/v${CHECKER_VERSION}/local-php-security-checker_${CHECKER_VERSION}_linux_$(echo ${TARGETPLATFORM} | sed -E 's/linux\///g')"; \
   chmod +x /local-php-security-checker;

ENTRYPOINT [ "/local-php-security-checker" ]
