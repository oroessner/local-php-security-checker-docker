# Local PHP Security Checker Docker Container

Run [Local PHP Security Checker](https://github.com/fabpot/local-php-security-checker) via Docker.

This Repo is heavily inspired and in some parts even copied
from https://github.com/pplotka/local-php-security-checker-github-actions but follows the versioning of the main
library.

This image is a multi-arch build for `amd64` and `arm64`.

## How to use

In the directory containing your `composer.lock` run:

```shell
docker run --rm -v $(pwd):/app registry.gitlab.com/oroessner/local-php-security-checker-docker:latest --path=/app
```

run

```shell
docker run --rm -v $(pwd):/app --workdir=/app registry.gitlab.com/oroessner/local-php-security-checker-docker:latest --help
```

to see all options.

## gitlab-ci example

If you want to use this image at gitlab-ci, use this task yaml:

```yaml
# .gitlab-ci.yml

local-php-security-checker:
  image: 
    name: registry.gitlab.com/oroessner/local-php-security-checker-docker:latest
    entrypoint: [""]
  script: /local-php-security-checker --path="${CI_PROJECT_DIR}"
```
