#!/usr/bin/env sh
architecture=$(arch)
version="${CHECKER_VERSION:-1.2.0}"
image_latest=registry.gitlab.com/oroessner/local-php-security-checker-docker:latest
image_versioned=registry.gitlab.com/oroessner/local-php-security-checker-docker:v${version}

if [ "$architecture" = "arm64" ] || [ "$CI" = true ]; then
    docker buildx build \
        --platform linux/arm64/v8,linux/amd64 \
        --no-cache --pull \
        --build-arg CHECKER_VERSION=${version} \
        -t "${image_latest}" \
        -t "${image_versioned}" \
        --push .
else
    docker build --no-cache --pull --build-arg CHECKER_VERSION=${version} -t "${image_latest}" -t "${image_versioned}" .
    docker push "${image_latest}"
    docker push "${image_versioned}"
fi
